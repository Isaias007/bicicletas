let Usuario = require("../../models/Usuarios");


exports.usuario_list = function (req, res) {

    Usuario.allusers(function (err, usuario) {
        if (err) {
            res.status(500).send(err.message);
        }

        res.status(200).json({
            Usuario: usuario
        });

    });

};



exports.usuario_reservar = function (req, res) {

    Usuario.findById(req.body.usuario_id, function (err, usuario) {

        if (err) res.status(500).send(err.message);

        console.log(usuario);

        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function (err) {

            console.log("Reservada!!");

            res.status(200).send();

        });

    });

};


exports.usuario_create = function (req, res) {


    let user = new Usuario({

        nombre: req.body.nombre

    });


    Usuario.add(user, function (err, newUser) {

        if (err) {
            res.status(500).send(err.message);
        }

        res.status(201).send(newUser);


    });
};


exports.usuario_delete = function (req, res) {

    let usuarioId = req.body._id;

    Usuario.removeById(usuarioId, function (err, usuarioId) {
        if (err) {
            res.status(500).send(err.message);
        };

        res.status(201).send(usuarioId);
    });

};


exports.usuario_update = function (req, res) {

    let filtro = req.body._id;

    Usuario.updateById(filtro, req.body, function (err, usuario) {
        if (err) {
            res.status(500).send(err.message);
        };
        usuario = req.body;
        res.status(201).send(usuario);
    });
}