let Reserva = require("../../models/Reservas");



exports.reserva_list = function (req, res) {

    Reserva.allreserva(function (err, reservas) {
        if (err) {
            res.status(500).send(err.message);
        }

        res.status(200).json({
            Reserva: reservas
        });

    });

};

exports.reserva_delete = function (req, res) {

    let reservaId = req.body.reservaId;
    console.log(reservaId);

    Reserva.removeById(reservaId, function (err, reservaId) {
        if (err) {
            res.status(500).send(err.message);
        };

        res.status(201).send(reservaId);
    });

};