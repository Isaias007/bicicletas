let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = function(req, res){

    Bicicleta.allBicis(function(err, bicis){
        if(err){
            res.status(500).send(err.message);
        }

        res.status(200).json ({
            bicicleta: bicis
        });

    });
    
};


exports.bicicleta_create = function(req, res){


    let bici = new Bicicleta({

        bicicletaID: req.body.bicicletaID,
       
        color: req.body.color,
       
        modelo: req.body.modelo,
       
        ubicacion: req.body.ubicacion
       
        });


    Bicicleta.add(bici, function(err, newBici){

        if(err){
            res.status(500).send(err.message);
        }

        res.status(201).send(newBici);


    });


    // El codigo 201 de http significa que la peticion de creado se a realizado con exito.

};



exports.bicicleta_delete = function(req,res){

    let biciId = req.body.bicicletaID;

    Bicicleta.removeById(biciId, function(err, biciId){
        if(err){
            res.status(500).send(err.message);
        };

        res.status(201).send(biciId);
    });

};


exports.bicicleta_update = function(req,res){

    let filtro = req.body.bicicletaID;


   Bicicleta.updateById(filtro, req.body, function(err, biciId){
       if(err){
           res.status(500).send(err.message);
       };
       biciId = req.body;
       res.status(201).send(biciId);
   });
}




