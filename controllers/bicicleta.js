// Creamos un metodo que tendra una funcion que llamara a la vista index.pug que mostrara todas las bicicletas.
let Bicicleta = require("../models/Bicicleta");

exports.bicicleta_list = function (req, res) {

    Bicicleta.allBicis(function (err, bicis) {
        if (err) {
            res.status(500).send(err.message);
        }

        res.render("bicicletas", { bicis: bicis });

    });
}


exports.bicicleta_create_get = function (req, res) {
    res.render("bicicletas/create");
}

// Creamos un metodo que creara una nueva bicicleta con la vista create.pug y redireccionara.
exports.bicicleta_create_post = function (req, res) {

    let bici = new Bicicleta({

        bicicletaID: req.body.bicicletaID,

        color: req.body.color,

        modelo: req.body.modelo,

        ubicacion: [req.body.latitud, req.body.longitud]

    });

    Bicicleta.add(bici, function (err, newBici) {

        if (err) {
            res.status(500).send(err.message);
        }
    });

    res.redirect("/bicicletas");
}

// Creamos un metodo que borrara la bicicicleta este usara una funcion creada en la parte de los models que sera removeByID que conseguira el 
// objeto que necesitamos para conseguir su id y borrarlo
exports.bicicleta_delete_post = function (req, res) {

    let biciId = req.body.bicicletaID;

    Bicicleta.removeById(biciId, function (err, biciId) {
        if (err) {
            res.status(500).send(err.message);
        };

        res.redirect("/bicicletas");
    });



}

// Este metodo es parecido al de arriba pero conseguimos el parametro por GET para poder realizar su edicion 

exports.bicicleta_update_get = function (req, res) {
    Bicicleta.findById(req.params.id, function (err, bici) {

        if (err) {
            res.status(500).send(err.message);
        };

        res.render("bicicletas/update", {bici:bici});
    });
   

}


// Este metodo es el que cambiara cada cosa del objeto directamente ya que consigue gracias al findByID y cambiamos cada atributo del objeto y volvemos a redirijir a la pagina
exports.bicicleta_update_post = function (req, res) {

    let filtro = req.params.id;


    Bicicleta.updateById(filtro, req.body, function (err, bici) {

        if (err) {
            res.status(500).send(err.message);
        };

        console.log(bici);
        res.redirect("/bicicletas");
    });

}




