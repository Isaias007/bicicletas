let Usuario = require("../models/Usuarios");
//LLamamos al modelo
module.exports = {

    //Primera funcion que sera la de list que tendra un find que es de mongoose
    list: function (req, res, next) {
        Usuario.find({}, function (err, usuarios) {
            if (err) {
            res.send(500, err.message);
            }else{
                res.render("usuarios/index", { usuarios: usuarios });
            }
            
        });
    },
    //Luego tendremos los updates que uno sera para pandarnos a la vista de edicion que sera el que pasaremos por get y luego tenemos el de post que sera el que usara el 
    //findByIdAndUpdate
    update_get: function (req, res, next) {
        Usuario.findById(req.params.id, function (err, usuario) {
            res.render("usuarios/update", { errors: {}, usuario: usuario });
        });
    },
    update: function (req, res, next) {
        let update_values = { nombre: req.body.nombre };
        Usuario.findByIdAndUpdate(req.params.id, update_values, function (err, usuario) {
            if (err) {
                console.log(err);
                res.render("usuario/update", { errors: err.errors, usuario: new Usuario({ nombre: req.body.nombre, email: req.body.email }) });
            } else {
                res.redirect("/usuarios");
                return;
            }
        });
    },
    // Estas seran para la creacion que sera igual que los updates pero con las funcionalidades de mongoose cambiadas a create 
    create_get: function (req, res, next) {
        res.render("usuarios/create", { errors: {}, usuario: new Usuario() });
    },
    create: function (req, res, next) {
        // Este codigo es la comparacion de las contraseñas para que sean iguales y se confirme
        if (req.body.password != req.body.confirm_password) {
            res.render("usuarios/create", { errors: { confirm_password: { message: "No coincide con el password introducido." } }, usuario: new Usuario({ nombre: req.body.nombre, email: req.body.email }) });
            return;
        }
        Usuario.create({ nombre: req.body.nombre, email: req.body.email, password: req.body.password }, function (err, nuevoUsario) {
            if (err) {
                res.render("usuarios/create", { errors: err.errors, usuario: new Usuario({ nombre: req.body.nombre, email: req.body.email }) });
            } else {
                nuevoUsario.enviar_email_bienvenida();
                res.redirect("/usuarios");
            }
        });
    },
    //Y esta ultima sera para poder borrar el usuario en cuestion
    delete: function (req, res, next) {
        Usuario.findByIdAndDelete(req.body.id, function (err) {
            if (err)
                next(err);
            else
                res.redirect("/usuarios");
        });
    }
};