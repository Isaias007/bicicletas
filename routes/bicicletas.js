// Con este fichero tendremos las rutas que crearemos para mostrar las bicis

//Llamamos a express
var express = require('express');
var router = express.Router();

//Creamos una variable donde tendra el controlador que sera llamada por una ruta get
let bicicletaController = require('../controllers/bicicleta');
router.get("/", bicicletaController.bicicleta_list);

// Cada ruta tendra un controlador asociado. 
router.get("/create", bicicletaController.bicicleta_create_get);
router.post("/create", bicicletaController.bicicleta_create_post);
router.post("/:id/delete", bicicletaController.bicicleta_delete_post);
router.get("/:id/update", bicicletaController.bicicleta_update_get);
router.post("/:id/update", bicicletaController.bicicleta_update_post);




module.exports = router;