let express = require('express');
let router = express.Router();
let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");


router.get("/", usuariosControllerAPI.usuario_list);
router.post("/create", usuariosControllerAPI.usuario_create);
router.post("/reservar", usuariosControllerAPI.usuario_reservar);
router.delete("/delete", usuariosControllerAPI.usuario_delete);
router.put("/update", usuariosControllerAPI.usuario_update);







module.exports = router;