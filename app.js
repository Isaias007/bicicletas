var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var Usuario = require("./models/Usuarios");
var Token = require("./models/Tokens");
var jwt = require("jsonwebtoken");





const passport = require("./config/passport");

const session = require("express-session");


const store = new session.MemoryStore;


//Require de mongoose
var mongoose = require('mongoose');



//Conexion a mongo



mongoose.connect('mongodb://192.168.31.5/red_bicicletas', { useUnifiedTopology: true, useNewUrlParser: true });

mongoose.Promise = global.Promise;

var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)

db.on("error", console.error.bind('Error de conexión con MongoDB'));




// Sitio de las rutas

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var authAPIRouter = require('./routes/api/auth');
var usuariosAPIRouter = require('./routes/api/usuarios');
var reservaAPIRouter = require('./routes/api/reserva');
var usuariosRouter = require("./routes/usuarios");
var tokenRouter = require("./routes/tokens");

var app = express();


app.use(session({

  cookie: { magAge: 240 * 60 * 60 * 1000 }, //Tiempo en milisegundos

  store: store,

  saveUninitialized: true,



  resave: "true",



  secret: "cualquier cosa no pasa nada 477447"

}));



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('secretkey', 'JWT_PWD_!!223344');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));













//////////////////////////////////////////Login




app.get("/login", function (req, res) {

  res.render("session/login");

});



app.post("/login", function (req, res, next) {

  passport.authenticate("local", function (err, usuario, info) {

    if (err) return next(err);

    if (!usuario) return res.render("session/login", { info });

    req.logIn(usuario, function (err) {

      if (err) return next(err);

      return res.redirect("/");

    });

  })(req, res, next);

});

app.get("/logout", function (req, res) {

  req.logOut(); //Limpiamos la sesión

  res.redirect("/");

});

app.get("/forgotPassword", function (req, res) {

  res.render("session/forgotPassword");


});

app.post("/forgotPassword", function (req, res) {

  Usuario.findOne({ email: req.body.email }, function (err, usuario) {

    if (!usuario) return res.render("session/forgotPassword", { info: { message: "No existe ese email en nuestra BBDD." } });



    usuario.resetPassword(function (err) {

      if (err) return next(err);

      console.log("session/forgotPasswordMessage");

    });

    res.render("session/forgotPasswordMessage");

  });

});


app.get("/resetPassword/:token", function (req, res, next) {

  Token.findOne({ token: req.params.token }, function (err, token) {

    if (!token) return res.status(400).send({ type: "not-verified", msg: "No existe un usuario asociado al token. Verifique que su token no haya expirado." });



    Usuario.findById(token._userId, function (err, usuario) {

      if (!usuario) return res.status(400).send({ msg: "No existe un usuario asociado al token." });

      res.render("session/resetPassword", { errors: {}, usuario: usuario });

    });

  });

});


app.post("/resetPassword", function (req, res) {

  if (req.body.password != req.body.confirm_password) {

    res.render("session/resetPassword", {
      errors: { confirm_password: { message: "No coincide con el password introducido." } },

      usuario: new Usuario({ email: req.body.email })
    });

    return;

  }

  Usuario.findOne({ email: req.body.email }, function (err, usuario) {

    usuario.password = req.body.password,

      usuario.save(function (err) {

        if (err) {

          res.render("session/resetPassword", { errors: err.errors, usuario: new Usuario({ email: req.body.email }) });

        } else {

          res.redirect("/login");

        }

      });

  });

});




app.use("/usuarios", usuariosRouter);
app.use("/token", tokenRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/auth', authAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/', indexRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);






// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});





function loggedIn(req, res, next) {

  if (req.user) {

    next();

  } else {

    console.log("Usuario no logueado");

    res.redirect("/login");

  }

}


function validarUsuario(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function (err, decoded) {
      if (err) {
        res.json({ status: "error", message: err.message, data: null });
  
      } else {
  
        req.body.userId = decoded.id;
  
        console.log('jwt verify: ' + decoded);
  
        next();
  
      }
  
    });
  }




module.exports = app;
