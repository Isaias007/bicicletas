
// Esquema mongoose

let mongoose = require("mongoose");
let Schema = mongoose.Schema;




let bicicletaSchema = new Schema({

 bicicletaID: Number,

 color: String,

 modelo: String,

 ubicacion: { type: [Number], index: true }
 // El index nos sirve para agilizar las busquedas ya que tenemos como una referencia en cada busqueda por ubicacion 

});




bicicletaSchema.statics.allBicis = function (cb) {
    return this.find({}, cb);

};



bicicletaSchema.statics.add = function(aBici, cb) {

    return this.create(aBici, cb);

};

// Funcion para borrar del array cada bicicleta mediante su id
bicicletaSchema.statics.removeById = function(aBiciID, cb){

    return this.remove({bicicletaID:aBiciID}, cb);

};

//Funcion para encontrar el id de cada bicicleta y si no existe te dara un mensaje de error
bicicletaSchema.statics.findById = function (aBiciID, cb){

    console.log(aBiciID);
    return this.findOne({_id:aBiciID}, cb);


};


bicicletaSchema.statics.updateById = function(filtro, update, cb){

    return this.findByIdAndUpdate(filtro, update, cb);

};















module.exports = mongoose.model ("Bicicleta", bicicletaSchema);
//module.exports = Bicicleta;