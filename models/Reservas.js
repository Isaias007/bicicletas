let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let reservaSchema = new Schema({

    desde: Date,

    hasta: Date,

    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta" },

    usuario: { type: mongoose.Schema.Types.ObjectId, ref: "Usuario" }

});



//Cuántos días está reservada la bicicleta

reservaSchema.methods.diasDeReserva = function () {

    return moment(this.hasta.diff(moment(this.desde), "days") + 1);

};

reservaSchema.statics.removeById = function (reservaId, cb) {

    return this.deleteOne({ _id: reservaId }, cb);


};

reservaSchema.statics.allreserva = function (cb) {
    return this.find({}, cb);

};

module.exports = mongoose.model("Reservas", reservaSchema);   